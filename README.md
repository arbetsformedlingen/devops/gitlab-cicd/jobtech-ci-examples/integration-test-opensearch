# Integration Testing with Ephemeral Services

In a CI/CD pipeline, it can be useful to have an [ephemeral
service](https://www.bunnyshell.com/blog/what-are-ephemeral-environment/),
for example a database server loaded with the necessary test data.

Here we show how to do this in Gitlab CI. In the example, an
[OpenSearch](https://opensearch.org/) server is started and then
loaded with test data, and finally a placeholder application using an
OpenSearch client performs a request to the database instance.

Have a look at [.gitlab-ci.yml](.gitlab-ci.yml) to see how it works.

## Implementation notes
It may seem a bit much to run all steps (retrieving data, loading db,
creating alias, running application) in the same job. But there is a
good reason for this: a Gitlab CI job is a process that will run on a
[Gitlab runner](https://docs.gitlab.com/runner/).  But if you have two
or more jobs in the same CI file, there is no guarantee that they will
be executed on the same runner. For this reason, it is impossible to
start a single ephemeral service in the CI file and expect it to be
reachable from all its jobs. The service needs to be started in the
same job in which it will be used.
