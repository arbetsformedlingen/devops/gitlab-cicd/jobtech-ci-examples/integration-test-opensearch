SHELL := /bin/bash

# Instructions:
#
# Create requirements.in to pin direct dependencies.
#
# Run `make install-deps` to generate requirements.txt
# where pinned

.PHONY: install-deps
install-deps: requirements.txt
	. venv/bin/activate \
	&& pip install -r requirements.txt


.PHONY: upgrade-deps
upgrade-deps: venv pip-tools requirements.in
	. venv/bin/activate \
	&& pip-compile --rebuild requirements.in


requirements.txt: venv pip-tools requirements.in
	. venv/bin/activate \
	&& pip-compile --rebuild requirements.in


.PHONY: pip-tools
pip-tools: venv
	. venv/bin/activate \
	&& pip list | grep pip-tools || pip install pip-tools==7.3.0


.PHONY: venv
venv:
	python -m venv venv
