from opensearchpy import OpenSearch
from dotenv import load_dotenv
import os


load_dotenv()


# Create the client with SSL/TLS and hostname verification disabled.
client = OpenSearch(
    hosts = [{'host': os.getenv('ES_HOST', 'search'), 'port': os.getenv('ES_PORT', 9200)}],
    http_compress = True, # enables gzip compression for request bodies
    use_ssl = False,
    verify_certs = False,
    ssl_assert_hostname = False,
    ssl_show_warn = False
)

q = 'Smed'
query = {
  'size': 5,
  'query': {
    'multi_match': {
      'query': q,
      'fields': ['label']
    }
  }
}

response = client.search(
    body = query,
    index = 'taxonomy'
)

print(response)
